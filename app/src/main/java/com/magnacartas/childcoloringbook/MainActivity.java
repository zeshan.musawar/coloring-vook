package com.magnacartas.childcoloringbook;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    WebView webView;
    private AdView adView;
    AdRequest adRequest;
    private InterstitialAd interstitialAd;
    AlertDialog.Builder builder;
    boolean connected = false;
    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = (WebView)findViewById(R.id.web_view);
        builder = new AlertDialog.Builder(this);
   //     if(connected) {
            webView.loadUrl("https://valencianguide.com/ch/book/HTML5/");
            //webView.loadUrl("https://www.google.com/");
            webView.getSettings().setJavaScriptEnabled(true); // enable javascript

            webView.getSettings().setPluginState(WebSettings.PluginState.ON);

            webView.setWebViewClient(new HelloWebViewClient());

  //      }
//        else {
//            builder.setMessage(R.string.check_inter).setTitle(R.string.app_name);
//
//            //Setting message manually and performing action on button click
//            builder.setMessage("Do you want to close this application ?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            finish();
//                            Toast.makeText(getApplicationContext(), "you choose yes action for alertbox",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//        }

        MobileAds.initialize(this, "ca-app-pub-3587555750058159~5571746524");
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(String.valueOf(R.string.Interstitial));
       adRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener(){
            public void onAdLoaded(){
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }
            }
        });


        adView = (AdView) findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

//        new Timer().scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                //your method
//                adView.loadAd(adRequest);
//                interstitialAd.loadAd(adRequest);
//            }
//        }, 0, 1000);//put here time 1000 milliseconds=1 second
//
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else {
            connected = false;
        }
    }
    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        interstitialAd.loadAd(adRequest);

    }
}